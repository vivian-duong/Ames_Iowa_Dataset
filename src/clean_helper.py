import pandas as pd


def obj_to_ordinal(feat):
    order = ['none','Po','Fa','TA','Gd','Ex']
    if feat.isin(order).any():
        s = pd.Categorical(feat)
        s = s.set_categories(order, ordered = True)
    else:
        s = pd.Categorical(feat)
    return s

def nan_to_str_none(ames_df):
    bsmt_nums = ['BsmtFinSF1', 'BsmtFinSF2', 'TotalBsmtSF', 
        'BsmtFullBath', 'BsmtHalfBath']
    garage_nums = ['GarageCars', 'GarageArea']
    
    
    cols_nan_add = {'Alley': None,
                     'BsmtQual': bsmt_nums,
                     'BsmtCond': bsmt_nums,
                     'BsmtExposure': bsmt_nums,
                     'BsmtFinType1': bsmt_nums,
                     'BsmtFinType2': bsmt_nums,
                     'FireplaceQu': None,
                     'GarageType': garage_nums,
                     'GarageYrBlt': garage_nums,
                     'GarageFinish': garage_nums,
                     'GarageQual': garage_nums,
                     'GarageCond': garage_nums,
                     'PoolQC': ['PoolArea'],
                     'Fence': None,
                     'MiscFeature': ['MiscVal']
                     }
    
    for qual, nums in cols_nan_add.items():
        if nums is not None:
            column = 1
            sum_nums = ames_df[nums].sum(axis = column)
            is_not_nan = ames_df[qual].isnull() == False
            to_str_none = (sum_nums + is_not_nan) == 0
        else:
            to_str_none = ames_df[qual].isnull()
            
        ames_df[qual][to_str_none] = 'none'
        
    return ames_df

def clean_ext_typos(ext_col):
    ext_col[ext_col == 'Brk Cmn'] = 'BrkComm'
    ext_col[ext_col == 'Wd Shng'] = 'WdShing'
    ext_col[ext_col == 'CmentBd'] = 'CemntBd'
    
    return ext_col

def bin_yrs(yr_col):
    bins = list(range(1800, 2011, 10))
    if is_string_dtype(yr_col):
        no_bin_nones = yr_col != 'none'
        yr_col[no_bin_nones] = pd.cut(yr_col[no_bin_nones], bins)
        binned_col = yr_col
    else:
        binned_col = pd.cut(yr_col, bins)
        
    return binned_col