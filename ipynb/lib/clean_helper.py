import numpy as np
import pandas as pd
from pandas.api.types import is_string_dtype

def obj_to_ordinal(feat):
    order = ['none','Po','Fa','TA','Gd','Ex']
    if feat.isin(order).any():
        s = pd.Categorical(feat)
        s = s.set_categories(order, ordered = True)
    else:
        s = pd.Categorical(feat)
    return s

def nan_to_str_none(ames_df):
    bsmt_nums = ['BsmtFinSF1', 'BsmtFinSF2', 'TotalBsmtSF', 
        'BsmtFullBath', 'BsmtHalfBath']
    garage_nums = ['GarageCars', 'GarageArea']
    
    
    cols_nan_add = {'Alley': None,
                     'BsmtQual': bsmt_nums,
                     'BsmtCond': bsmt_nums,
                     'BsmtExposure': bsmt_nums,
                     'BsmtFinType1': bsmt_nums,
                     'BsmtFinType2': bsmt_nums,
                     'FireplaceQu': None,
                     'GarageType': garage_nums,
                     'GarageYrBlt': garage_nums,
                     'GarageFinish': garage_nums,
                     'GarageQual': garage_nums,
                     'GarageCond': garage_nums,
                     'PoolQC': ['PoolArea'],
                     'Fence': None,
                     'MiscFeature': ['MiscVal']
                     }
    
    for qual, nums in cols_nan_add.items():
        if nums is not None:
            column = 1
            sum_nums = ames_df[nums].sum(axis = column)
            is_not_nan = ames_df[qual].isnull() == False
            to_str_none = (sum_nums + is_not_nan) == 0
        else:
            to_str_none = ames_df[qual].isnull()
            
        ames_df[qual][to_str_none] = 'none'
        
    return ames_df

def clean_ext_typos(ext_col):
    ext_col[ext_col == 'Brk Cmn'] = 'BrkComm'
    ext_col[ext_col == 'Wd Shng'] = 'WdShing'
    ext_col[ext_col == 'CmentBd'] = 'CemntBd'
    
    return ext_col

def bin_yrs(yr_col):
    bins = list(range(1800, 2011, 10))
    if is_string_dtype(yr_col):
        no_bin_nones = yr_col != 'none'
        yr_col[no_bin_nones] = pd.cut(yr_col[no_bin_nones], bins)
        binned_col = yr_col
    else:
        binned_col = pd.cut(yr_col, bins)
        
    return binned_col

ordinal_f = ['MSSubClass' ,'OverallQual',
             'OverallCond',
             'FullBath','HalfBath',
             'BedroomAbvGr','KitchenAbvGr',
             'TotRmsAbvGrd','Fireplaces',
             'MoSold', 'BsmtCond', 'BsmtQual',
             'ExterCond', 'ExterQual',
             'GarageQual', 'GarageCond',
             'HeatingQC', 'KitchenQual',
             'PoolQC']

def clean_years(year_df):
    ### drop years that don't make sense; years greater than 2010
    ### dataset only includes homes sold btwn 2006 and 2010
    for f in year_df.columns:
        is_no_yr = year_df[f].isnull() + (year_df[f].astype('str') == 'none') 
        exists_yr = is_no_yr == False
        to_nan =  year_df[f][exists_yr].apply(lambda x: int(x) > 2010)
        to_replace = year_df[f][exists_yr][to_nan].tolist()
        year_df[f] = year_df[f].replace(to_replace, [np.nan] * len(to_replace))
        clean_years = year_df
        
    return clean_years